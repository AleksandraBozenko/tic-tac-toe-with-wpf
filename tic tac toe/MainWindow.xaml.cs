﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace tic_tac_toe
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        int counter = 0;
        bool playerType = true;
        bool reset = false;


        private void checkWinner(bool player)
        {
            if (
                (Button0_0.Content == Button0_1.Content && Button0_0.Content == Button0_2.Content && Button0_0.Content != null) ||
                (Button1_0.Content == Button1_1.Content && Button1_0.Content == Button1_2.Content && Button1_0.Content != null) ||
                (Button2_0.Content == Button2_1.Content && Button2_0.Content == Button2_2.Content && Button2_0.Content != null) ||
                (Button0_0.Content == Button1_0.Content && Button0_0.Content == Button2_0.Content && Button0_0.Content != null) ||
                (Button0_1.Content == Button1_1.Content && Button0_1.Content == Button2_1.Content && Button0_1.Content != null) ||
                (Button0_2.Content == Button1_2.Content && Button0_2.Content == Button2_2.Content && Button0_2.Content != null) ||
                (Button0_0.Content == Button1_1.Content && Button0_0.Content == Button2_2.Content && Button0_0.Content != null) ||
                (Button0_2.Content == Button1_1.Content && Button0_2.Content == Button2_0.Content && Button0_2.Content != null)
                )
            {

                player = !player;

                if (player)
                {
                    MessageBox.Show("Congratulations X");
                    reset = true;
                }
                else
                {
                    MessageBox.Show("Congratulations O");
                    reset = true;
                }
            }

        }

        private void resetFields()
        {
            Button0_0.Content = null;
            Button1_0.Content = null;
            Button2_0.Content = null;
            Button0_1.Content = null;
            Button1_1.Content = null;
            Button2_1.Content = null;
            Button0_2.Content = null;
            Button1_2.Content = null;
            Button2_2.Content = null;
        }
        private void gameStatus()
        {
            counter++;
            checkWinner(playerType);

            if (counter == 9)
            {
                MessageBox.Show("Nobody won");
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (((Button)sender).Content != null)
            {
                return;
            }
            {
                if (playerType)
                {
                    ((Button)sender).Content = "X";
                    playerType = false;
                }
                else
                {
                    ((Button)sender).Content = "O";
                    playerType = true;
                }
                gameStatus();

                if (reset)
                {
                    resetFields();
                    counter = 0;
                    reset = false;
                }

            }
        }
    }
}
